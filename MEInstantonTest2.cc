// -*- C++ -*-
//
// This is the implementation of the non-inlined, non-templated member
// functions of the MEInstantonTest2 class.
//

#include "MEInstantonTest2.h"
#include "ThePEG/Interface/ClassDocumentation.h"
#include "ThePEG/EventRecord/Particle.h"
#include "ThePEG/Repository/UseRandom.h"
#include "ThePEG/Repository/EventGenerator.h"
#include "ThePEG/Utilities/DescribeClass.h"


#include "ThePEG/Persistency/PersistentOStream.h"
#include "ThePEG/Persistency/PersistentIStream.h"

using namespace Herwig;

MEInstantonTest2::MEInstantonTest2() {}

MEInstantonTest2::~MEInstantonTest2() {}

IBPtr MEInstantonTest2::clone() const {
  return new_ptr(*this);
}

IBPtr MEInstantonTest2::fullclone() const {
  return new_ptr(*this);
}

double MEInstantonTest2::me2() const {
  cout << "me2" << endl;
  return 1.;
}

multimap<tcPDPair,tcPDVector> MEInstantonTest2::processes() const {

  /*
  cout << "processes" << endl;
  tcPDPtr ep = getParticleData(ParticleID::eplus);
  tcPDPtr em = getParticleData(ParticleID::eminus);
  tcPDPtr g = getParticleData(ParticleID::g);
  tcPDPair incoming = make_pair(ep, em);
  tcPDVector outgoing;
  outgoing.push_back(g);
  outgoing.push_back(g);
  multimap<tcPDPair,tcPDVector> processmap;
  processmap.insert(make_pair(incoming,outgoing));
  return processmap;
  */

  // baryon number violation


  tcPDPtr ep = getParticleData(ParticleID::eplus);
  tcPDPtr em = getParticleData(ParticleID::eminus);
  tcPDPtr u = getParticleData(ParticleID::u);
  tcPDPtr d = getParticleData(ParticleID::d);
  tcPDPtr mum = getParticleData(ParticleID::muminus);
  tcPDPair incoming = make_pair(ep, em);
  tcPDVector outgoing;
  outgoing.push_back(u);
  outgoing.push_back(u);
  outgoing.push_back(d);
  outgoing.push_back(mum);
  multimap<tcPDPair,tcPDVector> processmap;
  processmap.insert(make_pair(incoming,outgoing));
  return processmap;

}

list<BlobMEBase::ColourConnection> MEInstantonTest2::colourConnections() const {

  /*
  cout << "colourcon" << endl;
  list<BlobMEBase::ColourConnection> res;
  BlobMEBase::ColourConnection first;
  first.addColour(2); first.addAntiColour(3);
  BlobMEBase::ColourConnection second;
  second.addColour(3); second.addAntiColour(2);
  res.push_back(first); res.push_back(second);
  return res;
  */

  // baryon number violation

  list<BlobMEBase::ColourConnection> res;
  BlobMEBase::ColourConnection first;
  first.addColour(2); first.addColour(3); first.addColour(4);
  res.push_back(first);
  return res;

}

size_t MEInstantonTest2::nOutgoing() const {
  /*
  cout << "noutgoing" << endl;
  return 2;
  */
  // baryon number violation
  return 3;
}

// If needed, insert default implementations of virtual function defined
// in the InterfacedBase class here (using ThePEG-interfaced-impl in Emacs).


void MEInstantonTest2::persistentOutput(PersistentOStream &) const {
  // *** ATTENTION *** os << ; // Add all member variable which should be written persistently here.
}

void MEInstantonTest2::persistentInput(PersistentIStream &, int) {
  // *** ATTENTION *** is >> ; // Add all member variable which should be read persistently here.
}


// *** Attention *** The following static variable is needed for the type
// description system in ThePEG. Please check that the template arguments
// are correct (the class and its base class), and that the constructor
// arguments are correct (the class name and the name of the dynamically
// loadable library where the class implementation can be found).
DescribeClass<MEInstantonTest2,Herwig::BlobME>
  describeHerwigMEInstantonTest2("Herwig::MEInstantonTest2", "MEInstantonTest2.so");

void MEInstantonTest2::Init() {

  static ClassDocumentation<MEInstantonTest2> documentation
    ("There is no documentation for the MEInstantonTest2 class");

}

