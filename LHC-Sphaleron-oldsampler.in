
# -*- ThePEG-repository -*-

##################################################
# Example generator based on LHC parameters
# usage: Herwig read LHC.in
##################################################

read snippets/PPCollider.in

##################################################
# Technical parameters for this run
##################################################

cd /Herwig/EventHandlers 
set EventHandler:CascadeHandler        NULL 
set EventHandler:HadronizationHandler  NULL 
set EventHandler:DecayHandler          NULL 
# The handler for multiple parton interactions 
set /Herwig/Shower/ShowerHandler:MPIHandler       NULL
set EventHandler:Weighted Yes

cd /Herwig/Generators
set EventGenerator:NumberOfEvents 10000000
set EventGenerator:RandomNumberGenerator:Seed 31122001
set EventGenerator:PrintEvent 1000
set EventGenerator:DebugLevel 1
set EventGenerator:MaxErrors 10000

cd /Herwig/Samplers

##################
# NEW SAMPLER
##################
#create Herwig::BlobSampler BlobSampler
#set BlobSampler:UpdateAfter 1000
#set /Herwig/EventHandlers/EventHandler:Sampler BlobSampler
#create Herwig::BinBlobSampler FlatBinBlobSampler
#set FlatBinBlobSampler:InitialPoints 10000
#set /Herwig/Samplers/FlatBinBlobSampler:NIterations 1
#set /Herwig/Samplers/FlatBinBlobSampler:InitialPoints 10000
#set /Herwig/Samplers/BlobSampler:BinBlobSampler /Herwig/Samplers/FlatBinBlobSampler
#set /Herwig/Samplers/BlobSampler:Verbose Yes
#set /Herwig/Samplers/BlobSampler:AlmostUnweighted Yes
#set /Herwig/Samplers/BlobSampler:BinBlobSampler:Kappa 0.01

#############################
# the default sampler (flat)
#############################
set /Herwig/EventHandlers/EventHandler:Sampler Sampler
set /Herwig/Samplers/FlatBinSampler:NIterations 1
set /Herwig/Samplers/FlatBinSampler:InitialPoints 10000
set /Herwig/Samplers/Sampler:BinSampler /Herwig/Samplers/FlatBinSampler
set /Herwig/Samplers/Sampler:Verbose Yes
set /Herwig/Samplers/Sampler:AlmostUnweighted Yes
set /Herwig/Samplers/Sampler:BinSampler:Kappa 0.01



#set /Herwig/Samplers/NoSampler:WriteGridsOnFinish Yes
#set /Herwig/Samplers/NoSampler:MaxEnhancement 1.5
#set /Herwig/Samplers/NoSampler:FlatSubprocesses Yes
#set /Herwig/Samplers/NoSampler:ParallelIntegration No

cd /Herwig/Generators
########################
## sqrt(s)	      ##
########################
set /Herwig/Generators/EventGenerator:EventHandler:LuminosityFunction:Energy 100000.0
set EventGenerator:EventHandler:MaxLoop 10000000

cd /Herwig/Cuts
set JetKtCut:MinKT 20.0*GeV
set /Herwig/Cuts/Cuts:MHatMin 9000*GeV
set /Herwig/Cuts/Cuts:MHatMax 50000*GeV

cd /Herwig/Analysis
set Basics:CheckQuark false # removes warning that there are quarks in the final state in case hadronization is turned off
set /Herwig/Analysis/Basics:RelativeMomentumTolerance 1E-5

##########################
## LHAPDF		##
##########################

#create ThePEG::LHAPDF /Herwig/Partons/LHAPDF ThePEGLHAPDF.so
#set /Herwig/Partons/LHAPDF:PDFName PDF4LHC15_nlo_mc
#set /Herwig/Partons/LHAPDF:RemnantHandler /Herwig/Partons/HadronRemnants
#set /Herwig/Particles/p+:PDF /Herwig/Partons/LHAPDF
#set /Herwig/Particles/pbar-:PDF /Herwig/Partons/LHAPDF
#set /Herwig/Partons/PPExtractor:FirstPDF  /Herwig/Partons/LHAPDF
#set /Herwig/Partons/PPExtractor:SecondPDF /Herwig/Partons/LHAPDF
# We would recommend the shower uses the default PDFs with which it was tuned.
# However it can be argued that the same set as for the sample should be used for
# matched samples, i.e. MC@NLO (and less so POWHEG)
#set /Herwig/Shower/ShowerHandler:PDFA /Herwig/Partons/LHAPDF
#set /Herwig/Shower/ShowerHandler:PDFB /Herwig/Partons/LHAPDF

##################################################
# Matrix Elements for hadron-hadron collisions 
# (by default only gamma/Z switched on)
##################################################


# The MAMBO phase space
cd /Herwig/MatrixElements/Matchbox/Phasespace
create Herwig::MamboPhasespace MamboPS
set MamboPS:CouplingData PhasespaceCouplings

cd /Herwig/MatrixElements/
create Herwig::MESphaleron MESphaleron Sphalerons.so

insert SubProcess:MatrixElements[0] MESphaleron
# Choose the phase space generator
set MESphaleron:Phasespace /Herwig/MatrixElements/Matchbox/Phasespace/MamboPS
#set MESphaleron:Phasespace /Herwig/MatrixElements/Matchbox/Phasespace/InvertiblePhasespace

set MESphaleron:NAdditionalMin 35
set MESphaleron:NAdditional 60

# fix the number of Higgses to be even
set MESphaleron:EvenNHiggs Yes
# binomial dist-n for the W vs Z gauge bosons 
set MESphaleron:BinomialGaugeBosons Yes
set MESphaleron:ProbWvsZ 0.6666666666666
# binomial dist-n for Z vs gamma 
set MESphaleron:BinomialNeutralGaugeBosons Yes
# binomial distribution for Higgses-other gauge bosons
set MESphaleron:BinomialHiggses No
# probability of obtaining a Higgs versus other gauge bosons
set MESphaleron:ProbH 0.0625

set MESphaleron:NPhotonMax 0
set MESphaleron:NZMax 10
set MESphaleron:NHiggsMax 0

set MESphaleron:NPhotonMin 0
set MESphaleron:NZMin 0
set MESphaleron:NHiggsMin 0

# set the sphaleron scale
set MESphaleron:SphaleronScale 9000.*GeV
set MESphaleron:SphaleronScaleMax 50000.*GeV

# set the process type
#set MESphaleron:ProcessType MultiGauge
#set MESphaleron:ProcessType SphaleronLight
set MESphaleron:ProcessType Sphaleron

# set the pre-factor in front of the ME
set MESphaleron:Prefactor 1.

# use all sub-processes or sample a few of them? (NRandom, All)
set MESphaleron:SubSampling NRandom 
set MESphaleron:NRandom 1

# how to generate the sub-processes
set MESphaleron:ProcessGeneration HardCoded
#set MESphaleron:ProcessGeneration OnTheFly

# how to set the boson multiplicity weights
#set MESphaleron:NumBosonParam Flat
set MESphaleron:NumBosonParam Gaussian

# LOME options (incompatible with "Gaussian" option of NumBosonParam")
set MESphaleron:LOME No
set MESphaleron:AdditionalPSFactors No
set MESphaleron:ReinstatePSFactor No


cd /Herwig/Analysis
#library MultiWeight.so
#create Herwig::MultiWeight /Herwig/Analysis/MultiWeight
#insert /Herwig/Generators/EventGenerator:AnalysisHandlers 0 /Herwig/Analysis/MultiWeight
cd /Herwig/Analysis
create ThePEG::RivetAnalysis RivetAnalysis RivetAnalysis.so
insert /Herwig/Analysis/RivetAnalysis:Analyses 0 MC_Sphaleron_Inclusive
insert /Herwig/Generators/EventGenerator:AnalysisHandlers 0 RivetAnalysis

cd /Herwig/Generators

##################################################
# Save run for later usage with 'Herwig run'
##################################################
saverun LHC-Sphaleron-oldsampler EventGenerator
