// -*- C++ -*-
//
// This is the implementation of the non-inlined, non-templated member
// functions of the MEInstanton class.
//

#include "MEInstanton.h"
#include "ThePEG/Interface/ClassDocumentation.h"
#include "ThePEG/EventRecord/Particle.h"
#include "ThePEG/Repository/UseRandom.h"
#include "ThePEG/Repository/EventGenerator.h"
#include "ThePEG/Utilities/DescribeClass.h"


#include "ThePEG/Persistency/PersistentOStream.h"
#include "ThePEG/Persistency/PersistentIStream.h"

using namespace Herwig;

MEInstanton::MEInstanton()
  : theNFamilies(3) {}

MEInstanton::~MEInstanton() {}

IBPtr MEInstanton::clone() const {
  return new_ptr(*this);
}

IBPtr MEInstanton::fullclone() const {
  return new_ptr(*this);
}

size_t MEInstanton::nOutgoing() const {

  // return the lowest number of final state particles without additional
  // bosons radiated

  // nAdditional() is the number of outgoing additional gluons and can be set
  // via the input file

}

multimap<tcPDPair,tcPDVector> MEInstanton::processes() const {

  // getParticleData(PDG Id)
  // getParticleData(PDT::Z0)

}

double MEInstanton::me2() const {

  // lastSHat()

  // mePartonData() and meMomenta() to decide what process has been used;
  // additional storage for decision which of the legs came from the DIS
  // splitting

  // numbering is always 0 1 are incoming, then outgoing follow

}

list<BlobMEBase::ColourConnection> MEInstanton::colourConnections() const {


}

// If needed, insert default implementations of virtual function defined
// in the InterfacedBase class here (using ThePEG-interfaced-impl in Emacs).


void MEInstanton::persistentOutput(PersistentOStream&) const {
}

void MEInstanton::persistentInput(PersistentIStream&, int) {
}


// *** Attention *** The following static variable is needed for the type
// description system in ThePEG. Please check that the template arguments
// are correct (the class and its base class), and that the constructor
// arguments are correct (the class name and the name of the dynamically
// loadable library where the class implementation can be found).
DescribeClass<MEInstanton,Herwig::BlobME>
  describeHerwigMEInstanton("Herwig::MEInstanton", "MEInstanton.so");

void MEInstanton::Init() {

  static ClassDocumentation<MEInstanton> documentation
    ("There is no documentation for the MEInstanton class");

}

