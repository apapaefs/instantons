 # -*- ThePEG-repository -*-

##################################################
# Example generator based on LHC parameters
# usage: Herwig read LHC.in
##################################################

read snippets/PPCollider.in

##################################################
# Technical parameters for this run
##################################################

cd /Herwig/EventHandlers 
set EventHandler:CascadeHandler        NULL 
set EventHandler:HadronizationHandler  NULL 
set EventHandler:DecayHandler          NULL 
# The handler for multiple parton interactions 
set /Herwig/Shower/ShowerHandler:MPIHandler       NULL
#set EventHandler:Weighted Yes

cd /Herwig/Generators
set EventGenerator:NumberOfEvents 10000000
set EventGenerator:RandomNumberGenerator:Seed 31122001
set EventGenerator:PrintEvent 1000
set EventGenerator:DebugLevel 1
set EventGenerator:MaxErrors 10000

# set the Bin sampler to flat.
set /Herwig/Samplers/FlatBinSampler:NIterations 2
set /Herwig/Samplers/FlatBinSampler:InitialPoints 1000
set /Herwig/Samplers/Sampler:BinSampler /Herwig/Samplers/FlatBinSampler
set /Herwig/Samplers/Sampler:Verbose Yes

set /Herwig/Samplers/Sampler:AlmostUnweighted Yes
set /Herwig/Samplers/Sampler:BinSampler:Kappa 0.01
#set /Herwig/Samplers/Sampler:WriteGridsOnFinish Yes
#set /Herwig/Samplers/Sampler:MaxEnhancement 1.5
#set /Herwig/Samplers/Sampler:FlatSubprocesses Yes
#set /Herwig/Samplers/Sampler:ParallelIntegration No


########################
## sqrt(s)	      ##
########################
set /Herwig/Generators/EventGenerator:EventHandler:LuminosityFunction:Energy 1000000.0
set EventGenerator:EventHandler:MaxLoop 10000000

set EventGenerator:EventHandler:Sampler /Herwig/Samplers/Sampler

cd /Herwig/Cuts
set JetKtCut:MinKT 20.0*GeV
set /Herwig/Cuts/Cuts:MHatMin 9000*GeV
set /Herwig/Cuts/Cuts:MHatMax 90000*GeV

cd /Herwig/Analysis
set Basics:CheckQuark false # removes warning that there are quarks in the final state in case hadronization is turned off
set /Herwig/Analysis/Basics:RelativeMomentumTolerance 1E-3

##################################################
# Matrix Elements for hadron-hadron collisions 
# (by default only gamma/Z switched on)
##################################################


# The MAMBO phase space
cd /Herwig/MatrixElements/Matchbox/Phasespace
create Herwig::MamboPhasespace MamboPS
set MamboPS:CouplingData PhasespaceCouplings

cd /Herwig/MatrixElements/
create Herwig::MESphaleron MESphaleron Sphalerons.so

insert SubProcess:MatrixElements[0] MESphaleron
# Choose the phase space generator
set MESphaleron:Phasespace /Herwig/MatrixElements/Matchbox/Phasespace/MamboPS

#set MESphaleron:Phasespace /Herwig/MatrixElements/Matchbox/Phasespace/InvertiblePhasespace
#set /Herwig/MatrixElements/Matchbox/Phasespace/InvertiblePhasespace:Nnormalise 25
#set /Herwig/MatrixElements/Matchbox/Phasespace/InvertiblePhasespace:RenormN Flat

set MESphaleron:NAdditionalMin 32
set MESphaleron:NAdditional 110

# fix the number of Higgses to be even
set MESphaleron:EvenNHiggs Yes
# binomial dist-n for the W vs Z gauge bosons 
set MESphaleron:BinomialGaugeBosons Yes
set MESphaleron:ProbWvsZ 0.6666666666666
# binomial dist-n for Z vs gamma 
set MESphaleron:BinomialNeutralGaugeBosons Yes
# binomial distribution for Higgses-other gauge bosons
set MESphaleron:BinomialHiggses Yes
# probability of obtaining a Higgs versus other gauge bosons
set MESphaleron:ProbH 0.0625

set MESphaleron:NPhotonMax 0
set MESphaleron:NZMax 0
set MESphaleron:NHiggsMax 0

set MESphaleron:NPhotonMin 0
set MESphaleron:NZMin 0
set MESphaleron:NHiggsMin 0


# set the sphaleron scale
set MESphaleron:SphaleronScale 9000.*GeV
set MESphaleron:SphaleronScaleMax 90000.*GeV

# set the process type
#set MESphaleron:ProcessType MultiGauge
#set MESphaleron:ProcessType SphaleronLight
set MESphaleron:ProcessType Sphaleron

# set the pre-factor in front of the ME
set MESphaleron:Prefactor 1.

# use all sub-processes or sample a few of them? (NRandom, All)
set MESphaleron:SubSampling NRandom 
set MESphaleron:NRandom 2

# how to generate the sub-processes
set MESphaleron:ProcessGeneration HardCoded
#set MESphaleron:ProcessGeneration OnTheFly

# how to set the boson multiplicity weights
#set MESphaleron:NumBosonParam Flat
set MESphaleron:NumBosonParam Gaussian

# LOME options (incompatible with "Gaussian" option of NumBosonParam")
set MESphaleron:LOME No
set MESphaleron:AdditionalPSFactors No
set MESphaleron:ReinstatePSFactor No


cd /Herwig/Analysis
#library MultiWeight.so
#create Herwig::MultiWeight /Herwig/Analysis/MultiWeight
#insert /Herwig/Generators/EventGenerator:AnalysisHandlers 0 /Herwig/Analysis/MultiWeight
cd /Herwig/Analysis
create ThePEG::RivetAnalysis RivetAnalysis RivetAnalysis.so
insert /Herwig/Analysis/RivetAnalysis:Analyses 0 MC_Sphaleron_Inclusive
insert /Herwig/Generators/EventGenerator:AnalysisHandlers 0 RivetAnalysis

cd /Herwig/Generators

##################################################
# Save run for later usage with 'Herwig run'
##################################################
saverun LHC-Sphaleron EventGenerator
