// -*- C++ -*-
#include "Rivet/Analyses/MC_JetAnalysis.hh"
#include "Rivet/Projections/ZFinder.hh"
#include "Rivet/Projections/VetoedFinalState.hh"
#include "Rivet/Projections/IdentifiedFinalState.hh"
#include "Rivet/Projections/FastJets.hh"
#include "Rivet/Projections/MissingMomentum.hh"

namespace Rivet {

  /// @brief MC validation analysis for higgs pairs events (stable Higgses)
  class MC_Sphaleron_Inclusive : public MC_JetAnalysis {
  public:

    /// Default constructor
    MC_Sphaleron_Inclusive()
      : MC_JetAnalysis("MC_Sphaleron_Inclusive", 10, "Jets")
    {  }


    /// @name Analysis methods
    //@{

    /// Book histograms
    void init() {
      IdentifiedFinalState ifs(Cuts::abseta < 10.0 && Cuts::pT > 0*GeV);
      ifs.acceptId(-24);
      ifs.acceptId(23);
      ifs.acceptId(24);
      ifs.acceptId(22);
      ifs.acceptId(25);

      declare(ifs,"IFS");
      VetoedFinalState vfs;
      vfs.addVetoPairId(-24);
      vfs.addVetoPairId(23);
      vfs.addVetoPairId(24);
      vfs.addVetoPairId(22);
      vfs.addVetoPairId(25);
      
      double sqrts = sqrtS()>0. ? sqrtS() : 100000.;

      declare(FastJets(vfs, FastJets::ANTIKT, 0.4), "Jets");

      _h_nboson = bookHisto1D("nboson", 100, 0, 100.0);
      _h_nwboson = bookHisto1D("nwboson", 100, 0, 100.0);
      _h_nzboson = bookHisto1D("nzboson", 100, 0, 100.0);
      _h_ngamma = bookHisto1D("ngamma", 100, 0, 100.0);
      _h_nhiggs = bookHisto1D("nhiggs", 100, 0, 100.0);
      _h_Q = bookHisto1D("Q", 50, 0, 5E4);
      _h_totalmass = bookHisto1D("totalmass", 50, 0, 5E4);


      MC_JetAnalysis::init();
    }



    /// Do the analysis
    void analyze(const Event & e) {

      const GenEvent* evt = e.genEvent();
      double Q = evt->event_scale();
      
      const double weight = e.weight();

      
      const IdentifiedFinalState& ifs = apply<IdentifiedFinalState>(e, "IFS");
      Particles allp = ifs.particlesByPt();
      if (allp.empty()) vetoEvent;

      _h_nboson->fill(allp.size(), weight);
      FourMomentum totalmom(0.,0.,0.,0.);
      int nwboson(0), nzboson(0), ngamma(0), nhiggs(0);
      for(size_t a = 0; a < allp.size(); a++) {
	totalmom += allp[a].momentum();
	if(fabs(allp[a].pid())==24) { nwboson++; } 
	if(allp[a].pid()==23) { nzboson++; } 
	if(allp[a].pid()==22) { ngamma++; }
	if(allp[a].pid()==25) { nhiggs++; }
      }
      _h_Q->fill(Q, weight); 
      _h_totalmass->fill(totalmom.mass(), weight);
      _h_nwboson->fill(nwboson, weight);
      _h_nzboson->fill(nzboson, weight);
      _h_ngamma->fill(ngamma, weight);
      _h_nhiggs->fill(nhiggs, weight);
      MC_JetAnalysis::analyze(e);
    }


    /// Finalize
    void finalize() {
      scale(_h_nboson, 1./sumOfWeights());
      scale(_h_nwboson, 1./sumOfWeights());
      scale(_h_nzboson, 1./sumOfWeights());
      scale(_h_ngamma, 1./sumOfWeights());
      scale(_h_nhiggs, 1./sumOfWeights());
      scale(_h_Q, 1./sumOfWeights());
      scale(_h_totalmass,1./sumOfWeights());
      MC_JetAnalysis::finalize();
    }

    //@}


  private:

    /// @name Histograms
    //@{
    Histo1DPtr _h_nboson;
    Histo1DPtr _h_nwboson;
    Histo1DPtr _h_nzboson;
    Histo1DPtr _h_ngamma;
    Histo1DPtr _h_nhiggs;
    Histo1DPtr _h_totalmass;
    Histo1DPtr _h_Q;
    //@}

  };


  // The hook for the plugin system
  DECLARE_RIVET_PLUGIN(MC_Sphaleron_Inclusive);

}
