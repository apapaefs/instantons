// -*- C++ -*-
#include "Rivet/Analyses/MC_JetAnalysis.hh"
#include "Rivet/Projections/ZFinder.hh"
#include "Rivet/Projections/WFinder.hh"
#include "Rivet/Projections/VetoedFinalState.hh"
#include "Rivet/Projections/IdentifiedFinalState.hh"
#include "Rivet/Projections/FastJets.hh"
#include "Rivet/Projections/MissingMomentum.hh"
#include "Rivet/Projections/ChargedLeptons.hh"


namespace Rivet {

  /// @brief MC validation analysis for higgs pairs events (stable Higgses)
  class MC_Sphaleron : public MC_JetAnalysis {
  public:

    /// Default constructor
    MC_Sphaleron()
      : MC_JetAnalysis("MC_Sphaleron", 20, "Jets")
    {  _jetptcut = 70.*GeV;
     }


    /// @name Analysis methods
    //@{

    /// Book histograms
    void init() {
      IdentifiedFinalState ifs(Cuts::abseta < 10.0 && Cuts::pT > 0*GeV);
      ifs.acceptId(-24);
      ifs.acceptId(23);
      ifs.acceptId(24);
      ifs.acceptId(22);
      ifs.acceptId(25);

      declare(ifs,"IFS");
      VetoedFinalState vfs;
      vfs.addVetoPairId(-24);
      vfs.addVetoPairId(23);
      vfs.addVetoPairId(24);
      vfs.addVetoPairId(22);
      vfs.addVetoPairId(25);
      
      double sqrts = sqrtS()>0. ? sqrtS() : 14000.;
      
      declare(FastJets(vfs, FastJets::ANTIKT, 0.4), "Jets");

      _h_nboson = bookHisto1D("nboson", 100, 0, 100.0);
      _h_nwboson = bookHisto1D("nwboson", 50, 0, 50.0);
      _h_nzboson = bookHisto1D("nzboson", 50, 0, 50.0);
      _h_ngamma = bookHisto1D("ngamma", 50, 0, 50.0);
      _h_nhiggs = bookHisto1D("nhiggs", 50, 0, 50.0);
      _h_met = bookHisto1D("met", 100, 0, sqrts/4.);
      _h_ptlepton = bookHisto1D("ptlepton", 100, 0, sqrts/4.);
      _h_ptw = bookHisto1D("ptw", 100, 0, sqrts/4.);
      _h_ptz = bookHisto1D("ptz", 100, 0, sqrts/4.);

      FinalState fs(FinalState(-5.0, 5.0, 0*GeV));
      declare(MissingMomentum(fs), "MissingET");

      
      Cut cut = Cuts::abseta < 5.0 && Cuts::pT > 70.*GeV;
      
      ZFinder zeefinder(FinalState(), cut, PID::ELECTRON, 65*GeV, 115*GeV, 0.2, ZFinder::CLUSTERNODECAY, ZFinder::TRACK);
      declare(zeefinder, "ZeeFinder");
      VetoedFinalState zmminput;
      zmminput.addVetoOnThisFinalState(zeefinder);
      ZFinder zmmfinder(zmminput, cut, PID::MUON, 65*GeV, 115*GeV, 0.2, ZFinder::CLUSTERNODECAY, ZFinder::TRACK);
      declare(zmmfinder, "ZmmFinder");

      WFinder wefinder(fs, cut, PID::ELECTRON, 60.0*GeV, 100.0*GeV, 25.0*GeV, 0.2);
      declare(wefinder, "WeFinder");
      VetoedFinalState wminput;
      wminput.addVetoOnThisFinalState(wefinder);
      WFinder wmfinder(wminput, cut, PID::MUON, 60.0*GeV, 100.0*GeV, 25.0*GeV, 0.2);
      declare(wefinder, "WmFinder");

      ChargedLeptons lfs(FinalState(-5.0, 5.0, 70*GeV));
      declare(lfs, "LFS");
      
      MC_JetAnalysis::init();
    }



    /// Do the analysis
    void analyze(const Event & e) {
      const double weight = e.weight();

      const ZFinder& zeefinder = apply<ZFinder>(e, "ZeeFinder");
      const ZFinder& zmmfinder = apply<ZFinder>(e, "ZmmFinder");
      
      const WFinder& wefinder = apply<WFinder>(e, "WeFinder");
      const WFinder& wmfinder = apply<WFinder>(e, "WmFinder");


      //get all particles
      const IdentifiedFinalState& ifs = apply<IdentifiedFinalState>(e, "IFS");
      Particles allp = ifs.particlesByPt();
      if (allp.empty()) vetoEvent;

      int nwboson(0), nzboson(0), ngamma(0), nhiggs(0);
      for(size_t a = 0; a < allp.size(); a++) {
	if(allp[a].pid()==22 && allp[a].momentum().pT() > 70.*GeV) { ngamma++; }
      }
      
      nzboson = zmmfinder.bosons().size() +  zeefinder.bosons().size();
      nwboson = wmfinder.bosons().size() +  wefinder.bosons().size();

      foreach (const Particle& lepton, lfs.chargedLeptons()) {
	FourMomentum l=lepton.momentum();
	_h_ptlepton->fill(l.pT(), weight);
      }
      foreach (const Particle& zmm, zmmfinder.bosons()) {
	FourMomentum z = zmm.momentum();
	_h_ptz->fill(z.pT(), weight);
      }
      foreach (const Particle& zee, zeefinder.bosons()) {
	FourMomentum z = zee.momentum();
	_h_ptz->fill(z.pT(), weight);
      }
      foreach (const Particle& we, wefinder.bosons()) {
	FourMomentum w = we.momentum();
	_h_ptw->fill(w.pT(), weight);
      }
      foreach (const Particle& wm, wmfinder.bosons()) {
	FourMomentum w = wm.momentum();
	_h_ptw->fill(w.pT(), weight);
      }

      _h_nboson->fill(nzboson+nwboson+ngamma, weight);

      _h_nwboson->fill(nwboson, weight);
      _h_nzboson->fill(nzboson, weight);

      _h_ngamma->fill(ngamma, weight);

      _h_nhiggs->fill(nhiggs, weight);
      
      MC_JetAnalysis::analyze(e);
    }


    /// Finalize
    void finalize() {
      scale(_h_nboson, crossSection()/sumOfWeights());
      scale(_h_nwboson, crossSection()/sumOfWeights());
      scale(_h_nzboson, crossSection()/sumOfWeights());
      scale(_h_ngamma, crossSection()/sumOfWeights());
      scale(_h_nhiggs, crossSection()/sumOfWeights());
      
      scale(_h_ptw, crossSection()/sumOfWeights());
      scale(_h_ptz, crossSection()/sumOfWeights());
      scale(_h_ptlepton, crossSection()/sumOfWeights());

      MC_JetAnalysis::finalize();
    }

    //@}


  private:

    /// @name Histograms
    //@{
    Histo1DPtr _h_nboson;
    Histo1DPtr _h_nwboson;
    Histo1DPtr _h_nzboson;
    Histo1DPtr _h_ngamma;
    Histo1DPtr _h_nhiggs;
    Histo1DPtr _h_met;
    Histo1DPtr _h_ptlepton;
    Histo1DPtr _h_ptw;
    Histo1DPtr _h_ptz;
    Histo1DPtr _h_ptgamma;


    //@}

  };


  // The hook for the plugin system
  DECLARE_RIVET_PLUGIN(MC_Sphaleron);

}
