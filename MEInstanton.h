// -*- C++ -*-
#ifndef Herwig_MEInstanton_H
#define Herwig_MEInstanton_H
//
// This is the declaration of the MEInstanton class.
//

#include "Herwig/MatrixElement/BlobME.h"

namespace Herwig {

using namespace ThePEG;

/**
 * Here is the documentation of the MEInstanton class.
 *
 * @see \ref MEInstantonInterfaces "The interfaces"
 * defined for MEInstanton.
 */
class MEInstanton: public Herwig::BlobME {

public:

  /** @name Standard constructors and destructors. */
  //@{
  /**
   * The default constructor.
   */
  MEInstanton();

  /**
   * The destructor.
   */
  virtual ~MEInstanton();
  //@}

public:

  /**
   * Return the order in \f$\alpha_S\f$ in which this matrix element
   * is given.
   */
  virtual unsigned int orderInAlphaS() const {
    return UINT_MAX;
  }

  /**
   * Return the order in \f$\alpha_{EM}\f$ in which this matrix
   * element is given. Returns 0.
   */
  virtual unsigned int orderInAlphaEW() const {
    return 0;
  }

  /**
   * Return the matrix element for the kinematical configuation
   * previously provided by the last call to setKinematics(), suitably
   * scaled by sHat() to give a dimension-less number.
   */
  virtual double me2() const;

  /**
   * Return the possible processes this matrix element will be able to handle,
   * as a map incoming to outgoing; it is assumed that the number of outgoing
   * partons does not vary.
   */
  virtual multimap<tcPDPair,tcPDVector> processes() const;

  /**
   * Return the colour connections for the process as pairs of id's of
   * external legs connecting colour to anticolour; id's of incoming partons
   * (0 and 1) have the meaning of colour and anti-colour eversed (crossed to
   * the final state).
   */
  virtual list<BlobMEBase::ColourConnection> colourConnections() const;

  /**
   * Return the number of final state particles except the additional fermion
   * legs and gauge bosons
   */
  virtual size_t nOutgoing() const;

public:

  /** @name Functions used by the persistent I/O system. */
  //@{
  /**
   * Function used to write out object persistently.
   * @param os the persistent output stream written to.
   */
  void persistentOutput(PersistentOStream & os) const;

  /**
   * Function used to read in object persistently.
   * @param is the persistent input stream read from.
   * @param version the version number of the object when written.
   */
  void persistentInput(PersistentIStream & is, int version);
  //@}

  /**
   * The standard Init function used to initialize the interfaces.
   * Called exactly once for each class by the class description system
   * before the main function starts or
   * when this class is dynamically loaded.
   */
  static void Init();

protected:

  /** @name Clone Methods. */
  //@{
  /**
   * Make a simple clone of this object.
   * @return a pointer to the new object.
   */
  virtual IBPtr clone() const;

  /** Make a clone of this object, possibly modifying the cloned object
   * to make it sane.
   * @return a pointer to the new object.
   */
  virtual IBPtr fullclone() const;
  //@}


// If needed, insert declarations of virtual function defined in the
// InterfacedBase class here (using ThePEG-interfaced-decl in Emacs).


private:

  /**
   * The assignment operator is private and must never be called.
   * In fact, it should not even be implemented.
   */
  MEInstanton & operator=(const MEInstanton &);

};

}

#endif /* Herwig_MEInstanton_H */
