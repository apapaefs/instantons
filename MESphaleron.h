// -*- C++ -*-
#ifndef Herwig_MESphaleron_H
#define Herwig_MESphaleron_H
//
// This is the declaration of the MESphaleron class.
//

#include "Herwig/MatrixElement/BlobME.h"
#include "ThePEG/PDT/MatcherBase.h"
#include "Herwig/MatrixElement/ProductionMatrixElement.h"
#include "Herwig/Utilities/Kinematics.h"
#include "ThePEG/Handlers/StandardXComb.h"

namespace Herwig {

using namespace ThePEG;

/**
 * Here is the documentation of the MESphaleron class.
 *
 * @see \ref MESphaleronInterfaces "The interfaces"
 * defined for MESphaleron.
 */
class MESphaleron: public Herwig::BlobME {

public:

  /** @name Standard constructors and destructors. */
  //@{
  /**
   * The default constructor.
   */
  MESphaleron();

  /**
   * The destructor.
   */
  virtual ~MESphaleron();
  //@}

public:

  /**
   * Return the order in \f$\alpha_S\f$ in which this matrix element
   * is given.
   */
  virtual unsigned int orderInAlphaS() const {
    return 0;
  }

  /**
   * Return the order in \f$\alpha_{EM}\f$ in which this matrix
   * element is given. Returns 0.
   */
  virtual unsigned int orderInAlphaEW() const {
    return UINT_MAX;
  }

  /**
   * Return the matrix element for the kinematical configuation
   * previously provided by the last call to setKinematics(), suitably
   * scaled by sHat() to give a dimension-less number.
   */
  virtual double me2() const;

  /**
   * Return the possible processes this matrix element will be able to handle,
   * as a map incoming to outgoing; it is assumed that the number of outgoing
   * partons does not vary.
   */
  virtual multimap<tcPDPair,tcPDVector> processes() const;

  /**
   * Return the colour connections for the process as pairs of id's of
   * external legs connecting colour to anticolour; id's of incoming partons
   * (0 and 1) have the meaning of colour and anti-colour eversed (crossed to
   * the final state).
   */
  virtual list<BlobMEBase::ColourConnection> colourConnections() const;

  /**
   * Return the number of final state particles except the additional fermion
   * legs and gauge bosons
   */
  virtual size_t nOutgoing() const;

  /**
   * Return the number of families to consider
   */
  size_t nFamilies() const { return theNFamilies; }

  /**
   *
   */
  std::pair<tcPDPair,tcPDVector> string_to_process(string procstring) const;

  /**
   * count the number of bosons (Higgs bosons, Ws, Zs and photons)
   */
  void count_bosons() const;

  /**
   * calculate the additional phase-space factors 
   * for fermions
   */
  
  double get_fermion_additional_PS_factor() const;

  /**
   * calculate the additional phase-space factors 
   * for gauge bosons
   */
  double get_gaugeboson_additional_PS_factor() const;

  
  /**
   * the binomial probability function
   */

  double binomialfunct(double prob, int N1, int N2) const;


  
  /**
   * the function that gets the colour multiplicity factor for the current process
   */

  double get_colour_multiplicity() const;


  /**
   * function to help generate all the possible colour connections
   */
  vector<vector<int>> gen_line(vector<int> ncints, vector<vector<int>> ncints_rem) const;

  /**
   * returns true if the colour sinks/sources involve identical particles
   */
  bool is_antisymmetric(vector<int> partons, vector<int> line) const;

  
  /**
   * returns true if the colour sinks/sources involve identical particles
   * this version only checks for colour-connected top quarks.
   */
  bool is_antisymmetric_light(vector<int> partons, vector<int> line) const;

  /** 
   * fill processes from binary file
   */
  
  //void fill_processes() const;

 /** 
   * fill processes by reading the .dat file
   */
  
  void fill_processes_dat(string filestring) const;


  /*
   * number of bosons in the current process
   */
  mutable int _nw;
  mutable int _nh;
  mutable int _nz;
  mutable int _na;

  /*
   * the W mass
   */
  mutable double Wmass;

  /* 
   * a prefactor for the ME^2
   */
  mutable double prefactor;

  /*
   * the process type
   */
  mutable unsigned int proctype;
  
   /*
   * the type of process subsampling.
   */
  mutable unsigned int subsample;

  /*
   * the method for the generation of processes;
   */
  mutable unsigned int ProcessGeneration;
  
  /* 
   * the colour multiplicity factor for each process
   */
  mutable map<string,double> colour_multiplicity_map;

  /*
   * the parameters for the Gaussian representing the number of gauge boson parametrisation
   */
  mutable double NumBosonParamA1;
  mutable double NumBosonParamA2;
  mutable double NumBosonParamB1;
  mutable double NumBosonParamB2;

  /*
   * Remove the phase space factor related to the multiplicity of the final state: 
   * use this to put all multiplicities on the same footing. 
   */ 
  mutable bool ReinstatePSFactor;

  /*
   * Freeze the Gaussian multiplicity parameters beyond scale set by FreezeGaussianParamsScale.
   */
  
  mutable bool FreezeGaussianParams;

  /*
   * The scale to freeze the gaussian multiplicity parameters at
   */
  mutable Energy FreezeGaussianParamsScale;

  /*
   * Enforce Unitarity Limit or not after EnforceUnitarityLimitScale (i.e. make the partonic cross section scale as shat_UnitViol/shat beyond that scale).
   */
  mutable bool EnforceUnitarityLimit;

 /*
   * The scale (shat_UnitViol) at which to switch to ~ shat_UnitViol/shat; 
   */ 
  mutable Energy EnforceUnitarityLimitScale;
  
  /*
   * the probability to get W+/W- instead of neutral (Z0/gamma)
   */
  mutable double prob_W;

  /*
   * the probability to get Z0 instead of gamma
   */
  mutable double prob_Z;

  /*
   * the probability to get Higgs instead of Z0
   */
  mutable double prob_H;
  
  /* 
   * the maximum number of processes in the .dat file
   */ 
  mutable int NWC_DAT_MAX;
  
  /*
   * the number of additional particles
   */
  int theNAdditional;

  /*
   * the minimum number of additional particles
   */
  size_t theNAdditionalMin;

  /* 
   * if picking processes at random, how many to pick
   */
  size_t NRandom;

  /*
   * the type of parametrisation of the number of gauge bosons
   */
  mutable unsigned int NumBosonParam;

   /*
   * the maximum number of additional photons
   */
  int NPhotonMax;

   /*
   * the maximum number of additional Higgses
   */
  int NHiggsMax;

   /*
   * the maximum number of additional Zs
   */
  int NZMax;
  
   /*
   * the reduction to max number of additional photons
   */
  int NPhotonRed;

   /*
   * the reduction to max number of additional Higgses
   */
  int NHiggsRed;

   /*
   * the reduction to max number of additional Zs
   */
  int NZRed;

   /*
   * whether to restrict the number of Higgs bosons to even or not
   */
  bool EvenNHiggs;

  /*
   * the minimum number of Zs
   */
  int minzs;

  /*
   * the minimum number of photons
   */
  int minphotons;

  /*
   * the minimum number of higgses
   */
  int minhiggses;


  /* 
   * the array of hardcoded processes
   */
  
  mutable vector<vector<string>> process_array;


  /*
   * include the additional phase space factors from the LOME phase space
   */
  mutable bool AdditionalPSFactors;

    /*
   * include LOME 
   */
  mutable bool LOME;
  
  /*
   * whether to distribute neutral gauge bosons according to a binomial distribution with p(Z) = cos^2 theta_W
   */
  bool BinomialNeutralBosons;
    
  /*
   * whether to distribute charged vs neutral gauge bosons according to a binomial distribution with p(Z) = ProbWvsZ
   */
  bool BinomialGaugeBosons;

  /*
   * whether to distribute Higgs vs gauge bosons according to a binomial distribution with p(H) = ProbH
   */
  bool BinomialHiggses;

  /*
   * the vector that contains all possible colour connections
   */
  vector<vector<int>> linefull;
 
  /**
   * Return the matcher to identify the allowed beam particles for the first
   * beam
   */
  Ptr<MatcherBase>::tptr firstBeamMatcher() const { return theFirstBeamMatcher; }

  /**
   * Return the matcher to identify the allowed beam particles for the second
   * beam
   */
  Ptr<MatcherBase>::tptr secondBeamMatcher() const { return theSecondBeamMatcher; }

public:

  /** @name Functions used by the persistent I/O system. */
  //@{
  /**
   * Function used to write out object persistently.
   * @param os the persistent output stream written to.
   */
  void persistentOutput(PersistentOStream & os) const;

  /**
   * Function used to read in object persistently.
   * @param is the persistent input stream read from.
   * @param version the version number of the object when written.
   */
  void persistentInput(PersistentIStream & is, int version);
  //@}

  /**
   * The standard Init function used to initialize the interfaces.
   * Called exactly once for each class by the class description system
   * before the main function starts or
   * when this class is dynamically loaded.
   */
  static void Init();

    /**
   * Initialize this object after the setup phase before saving an
   * EventGenerator to disk.
   * @throws InitException if object could not be initialized properly.
   */
  virtual void doinit();

protected:

  /** @name Clone Methods. */
  //@{
  /**
   * Make a simple clone of this object.
   * @return a pointer to the new object.
   */
  virtual IBPtr clone() const;

  /** Make a clone of this object, possibly modifying the cloned object
   * to make it sane.
   * @return a pointer to the new object.
   */
  virtual IBPtr fullclone() const;
  //@}


// If needed, insert declarations of virtual function defined in the
// InterfacedBase class here (using ThePEG-interfaced-decl in Emacs).


private:

  /**
   * The assignment operator is private and must never be called.
   * In fact, it should not even be implemented.
   */
  MESphaleron & operator=(const MESphaleron &);

  /**
   * Return the number of families to consider
   */
  size_t theNFamilies;


  /* 
   * The Sphaleron scale
   */
  Energy theSphaleronScale;

  /*
   * The maximum of the sphaleron scale
   */
  Energy theSphaleronScaleMax; 
  

  /**
   * The matcher to identify the allowed beam particles of the first beam
   */
  Ptr<MatcherBase>::tptr theFirstBeamMatcher;

  /**
   * The matcher to identify the allowed beam particles of the second beam
   */
  Ptr<MatcherBase>::tptr theSecondBeamMatcher;

};

}

#endif /* Herwig_MESphaleron_H */
