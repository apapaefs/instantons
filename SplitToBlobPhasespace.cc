// -*- C++ -*-
//
// This is the implementation of the non-inlined, non-templated member
// functions of the SplitToBlobPhasespace class.
//

#include "SplitToBlobPhasespace.h"
#include "ThePEG/Interface/ClassDocumentation.h"
#include "ThePEG/EventRecord/Particle.h"
#include "ThePEG/Repository/UseRandom.h"
#include "ThePEG/Repository/EventGenerator.h"
#include "ThePEG/Utilities/DescribeClass.h"


#include "ThePEG/Persistency/PersistentOStream.h"
#include "ThePEG/Persistency/PersistentIStream.h"

using namespace Herwig;

SplitToBlobPhasespace::SplitToBlobPhasespace() {}

SplitToBlobPhasespace::~SplitToBlobPhasespace() {}

IBPtr SplitToBlobPhasespace::clone() const {
  return new_ptr(*this);
}

IBPtr SplitToBlobPhasespace::fullclone() const {
  return new_ptr(*this);
}

int SplitToBlobPhasespace::nDim() const {
  int nBlobLegs = 0;
  int nDOFSplitting = 0;
  // strip off the splittings and work out legs attached to the blob
  // and work out number of degrees of freedom needed for the splitting(s)
  // ...
  // return dimensionality of the blob phasespace
  return FlatInvertiblePhasespace::nDim(nBlobLegs) + nDOFSplitting;
}

double SplitToBlobPhasespace::generateTwoToNKinematics(const double* r,
						       vector<Lorentz5Momentum>& momenta) {
  int nDOFSplitting = 0;
  // work out number of degrees of freedom needed for the splitting(s)
  // ...
  double weight = 1.;
  // generate the splittings
  // use flat phasespace for remaining blob momenta
  vector<Lorentz5Momentum> blobMomenta;
  // initialize remaining and new incoming momenta into blobMomenta (check
  // mePartonData()), needs selection criterion which of the quarks/anti-quarks
  // should be attached to the splittings(s)
  weight *= FlatInvertiblePhasespace::generateTwoToNKinematics(r+nDOFSplitting,blobMomenta);
  // put back blob momenta into momenta
  return weight;
}

// If needed, insert default implementations of virtual function defined
// in the InterfacedBase class here (using ThePEG-interfaced-impl in Emacs).


void SplitToBlobPhasespace::persistentOutput(PersistentOStream &) const {
  // *** ATTENTION *** os << ; // Add all member variable which should be written persistently here.
}

void SplitToBlobPhasespace::persistentInput(PersistentIStream &, int) {
  // *** ATTENTION *** is >> ; // Add all member variable which should be read persistently here.
}


// *** Attention *** The following static variable is needed for the type
// description system in ThePEG. Please check that the template arguments
// are correct (the class and its base class), and that the constructor
// arguments are correct (the class name and the name of the dynamically
// loadable library where the class implementation can be found).
DescribeClass<SplitToBlobPhasespace,Herwig::FlatInvertiblePhasespace>
  describeHerwigSplitToBlobPhasespace("Herwig::SplitToBlobPhasespace", "SplitToBlobPhasespace.so");

void SplitToBlobPhasespace::Init() {

  static ClassDocumentation<SplitToBlobPhasespace> documentation
    ("There is no documentation for the SplitToBlobPhasespace class");

}

